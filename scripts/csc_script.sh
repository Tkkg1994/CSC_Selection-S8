#!/sbin/sh
# Written by Tkkg1994 for csc selection

if grep -q G93 /tmp/BLmodel; then
	mount /dev/block/platform/155a0000.ufs/by-name/SYSTEM /system
	mount /dev/block/platform/155a0000.ufs/by-name/USERDATA /data
	mount /dev/block/platform/155a0000.ufs/by-name/EFS /efs
else
	mount /dev/block/platform/11120000.ufs/by-name/SYSTEM /system
	mount /dev/block/platform/11120000.ufs/by-name/USERDATA /data
	mount /dev/block/platform/11120000.ufs/by-name/EFS /efs
fi

getprop ro.boot.bootloader >> /tmp/variant
ACTUAL_CSC=`cat /efs/imei/mps_code.dat`
ACTUAL_OMC=`cat /efs/imei/omcnw_code.dat`
if [ ! -f /system/csc/sales_code.dat ]; then
	SALES_CODE=`cat /system/omc/sales_code.dat`
else
	SALES_CODE=`cat /system/csc/sales_code.dat`
fi

if grep -q  'CSC=AME\|CSC=CHE\|CSC=CPA\|CSC=CWW\|CSC=DBT\|CSC=DTM\|CSC=FTM\|CSC=O2U\|CSC=SKC\|CSC=UPO\|CSC=VD2\|CSC=XAA\|CSC=XAC' /tmp/aroma/csc.prop; then
	echo "cscmulti.prop found"
	sed -i -- "s/CSC=//g" /tmp/aroma/cscmulti.prop
	NEW_CSC=`cat /tmp/aroma/cscmulti.prop`
else
	echo "csc.prop found"
	sed -i -- "s/CSC=//g" /tmp/aroma/csc.prop
	NEW_CSC=`cat /tmp/aroma/csc.prop`
fi

if [ ! -f /system/csc/sales_code.dat ]; then
	echo "OMC CSC found"
	if grep -q G955 /tmp/variant; then
		sed -i -- "s/G950/G955/g" /system/omc/CSCVersion.txt
		sed -i -- "s/G950/G955/g" /system/omc/SW_Configuration.xml
		find /system -type f -name 'omc*' | xargs sed -i "s/SM-G950F/SM-G955F/g"
		echo "Changed model to G955"
	else if grep -q G935 /tmp/variant; then
		sed -i -- "s/G950/G955/g" /system/omc/CSCVersion.txt
		sed -i -- "s/G950/G955/g" /system/omc/SW_Configuration.xml
		find /system -type f -name 'omc*' | xargs sed -i "s/SM-G950F/SM-G955F/g"
		echo "Changed model to G955"
	else if grep -q G950 /tmp/variant; then
		sed -i -- "s/G955/G950/g" /system/omc/CSCVersion.txt
		sed -i -- "s/G955/G950/g" /system/omc/SW_Configuration.xml
		find /system -type f -name 'omc*' | xargs sed -i "s/SM-G955F/SM-G950F/g"
		echo "Changed model to G950"
	else if grep -q G930 /tmp/variant; then
		sed -i -- "s/G955/G950/g" /system/omc/CSCVersion.txt
		sed -i -- "s/G955/G950/g" /system/omc/SW_Configuration.xml
		find /system -type f -name 'omc*' | xargs sed -i "s/SM-G955F/SM-G950F/g"
		echo "Changed model to G950"
	else
		echo "Not a supported model!"
	fi
	fi
	fi
	fi
else
	if grep -q G955 /tmp/variant; then
		sed -i -- "s/G950/G955/g" /system/CSCVersion.txt
		sed -i -- "s/G950/G955/g" /system/SW_Configuration.xml
		echo "Changed model to G955"
	else if grep -q G935 /tmp/variant; then
		sed -i -- "s/G950/G955/g" /system/CSCVersion.txt
		sed -i -- "s/G950/G955/g" /system/SW_Configuration.xml
		echo "Changed model to G955"
	else if grep -q G950 /tmp/variant; then
		sed -i -- "s/G955/G950/g" /system/CSCVersion.txt
		sed -i -- "s/G955/G950/g" /system/SW_Configuration.xml
		echo "Changed model to G950"
	else if grep -q G930 /tmp/variant; then
		sed -i -- "s/G955/G950/g" /system/CSCVersion.txt
		sed -i -- "s/G955/G950/g" /system/SW_Configuration.xml
		echo "Changed model to G950"
	else
		echo "Not a supported model!"
	fi
	fi
	fi
	fi
fi

if [ ! -f /system/csc/sales_code.dat ]; then
	echo "OMC CSC found"
	sed -i -- "s/$ACTUAL_CSC/$NEW_CSC/g" /efs/imei/mps_code.dat
	sed -i -- "s/$ACTUAL_OMC/$NEW_CSC/g" /efs/imei/omcnw_code.dat
	sed -i -- "s/$SALES_CODE/$NEW_CSC/g" /system/omc/sales_code.dat
	echo "flashing selected CSC"
else
	sed -i -- "s/$ACTUAL_CSC/$NEW_CSC/g" /efs/imei/mps_code.dat
	sed -i -- "s/$ACTUAL_OMC/$NEW_CSC/g" /efs/imei/omcnw_code.dat
	sed -i -- "s/$SALES_CODE/$NEW_CSC/g" /system/csc/sales_code.dat
	echo "flashing selected CSC"
fi

exit 10

