#!/sbin/sh
# Written by Tkkg1994

getprop ro.boot.bootloader >> /tmp/BLmodel

if grep -q G93 /tmp/BLmodel; then
	mount /dev/block/platform/155a0000.ufs/by-name/SYSTEM /system
else
	mount /dev/block/platform/11120000.ufs/by-name/SYSTEM /system
fi

cp /system/build.prop /tmp/version

if grep -q "ro.product.first_api_level=23" /tmp/version; then
	echo "S7 Rom detected"
	sed -i -- "s/s8/s7/g" /tmp/aroma/romcheck.prop
else if grep -q "ro.product.first_api_level=24" /tmp/version; then
	echo "S8 Rom detected"
else if grep -q "ro.product.first_api_level=25" /tmp/version; then
	echo "N8 Rom detected"
	sed -i -- "s/s8/n8/g" /tmp/aroma/romcheck.prop
else
	echo "Rom could not be detected successfully"
	sed -i -- "s/s8/nothing/g" /tmp/aroma/romcheck.prop
fi
fi
fi

exit 10

